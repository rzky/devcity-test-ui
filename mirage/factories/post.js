import { Factory, association, faker } from 'ember-cli-mirage';
import _ from 'lodash';

export default Factory.extend({
  type() {
    return faker.helpers.randomize(['text', 'image']);
  },

  date() {
    return faker.date.recent();
  },

  content() {
    if (this.type === 'text') {
      return faker.lorem.paragraph();
    } else {
      return '';
    }
  },

  media() {
    let type = ['cats', 'food', 'nightlife', 'people', 'city', 'sports', 'nature'];

    return _.times(_.random(2, 6), () => {
      return faker.image.imageUrl(252, 189, faker.helpers.randomize(type), true);
    });
  },

  user: association()
});
