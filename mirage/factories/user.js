import { Factory, faker } from 'ember-cli-mirage';
import _ from 'lodash';

export default Factory.extend({
  card() {
    return faker.helpers.contextualCard();
  },

  afterCreate(user, server) {
    // attach posts data to user profile
    let postNum = _.random(2, 5);
    server.createList('post', postNum, { user });

    // attach friends data to user profile
    server.create('relation', { user });
  }
});
