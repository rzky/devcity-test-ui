import { Factory, faker, association } from 'ember-cli-mirage';
import _ from 'lodash';

export default Factory.extend({
  friends() {
    return _.times(12, () => {
      return faker.helpers.contextualCard();
    });
  },

  common() {
    return _.times(4, () => {
      return faker.helpers.contextualCard();
    });
  },

  user: association()
});
