import DS from 'ember-data';
import { computed } from '@ember/object';

export default DS.Model.extend({
  card: DS.attr(),

  post: DS.hasMany(),
  relation: DS.belongsTo(),

  fullName: computed('firstName', 'lastName', function() {
    return `${this.firstName} ${this.lastName}`;
  })
});
