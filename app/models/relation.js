import DS from 'ember-data';

export default DS.Model.extend({
  friends: DS.attr(),
  common: DS.attr(),

  user: DS.belongsTo()
});
