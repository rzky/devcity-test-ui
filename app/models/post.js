import DS from 'ember-data';

export default DS.Model.extend({
  type: DS.attr(),
  date: DS.attr(),
  content: DS.attr(),
  media: DS.attr(),

  user: DS.belongsTo()
});
