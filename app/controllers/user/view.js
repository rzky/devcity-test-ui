import Controller from '@ember/controller';

export default Controller.extend({
  activeTab: 1,

  actions: {
    switchTab(tab) {
      this.set('activeTab', tab);
    }
  }
});
