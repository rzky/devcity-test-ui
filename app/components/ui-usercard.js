import Component from '@ember/component';
import { computed } from '@ember/object';

export default Component.extend({
  classNames: ['ui fluid card'],

  isFriend: null,
  isFriendStatus: computed('isFriend', function() {
    let isFriend = this.isFriend;

    if (isFriend) {
      return {
        icon: 'user times',
        message: 'Remove as friend'
      };
    } else {
      return {
        icon: 'user plus',
        message: 'Add as friend'
      };
    }
  }),

  isFollowing: null,
  isFollowingStatus: computed('isFollowing', function() {
    let isFollowing = this.isFollowing;

    if (isFollowing) {
      return {
        icon: 'rss',
        message: 'Unfollow'
      };
    } else {
      return {
        icon: 'rss',
        message: 'Follow'
      };
    }
  }),

  blocked: false,
  blockedStatus: computed('blocked', function() {
    let blocked = this.blocked;

    if (blocked) {
      return {
        icon: 'check circle outline',
        message: 'Unblock'
      };
    } else {
      return {
        icon: 'ban',
        message: 'Block'
      };
    }
  }),

  actions: {
    updateFriend() {
      this.toggleProperty('isFriend');
    },

    updateFollowing() {
      this.toggleProperty('isFollowing');
    },

    blockUser() {
      this.toggleProperty('blocked');
    }
  }
});
