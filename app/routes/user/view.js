import Route from '@ember/routing/route';
import RSVP from 'rsvp';

export default Route.extend({
  model(params) {
    return RSVP.hash({
      user: this.store.findRecord('user', params.user_id, { include: 'post,relation' })
    })
  }
});
