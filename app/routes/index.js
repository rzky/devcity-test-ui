import Route from '@ember/routing/route';
import RSVP from 'rsvp';

export default Route.extend({
  model() {
    return RSVP.hash({
      post: this.store.findAll('post', { include: 'user' }),
      user: this.store.findAll('user'),
      relation: this.store.findRecord('relation', 1)
    });
  }
});
